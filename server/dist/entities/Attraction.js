"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Attraction {
    constructor(attraction) {
        this.name = attraction.name;
        this.id = attraction.id;
    }
}
exports.default = Attraction;
